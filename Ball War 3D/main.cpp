#include <raylib.h>
#include <raymath.h>
#include <vector>
#include <ctime>

using namespace std;

/*------------------全局数据------------------*/
Camera3D camera;	//增加摄像头（观测点）
Vector3 ballPos;	//定义控制球坐标
float ballR;		//定义控制球半径
Vector2 maxSize = {1000, 1000};	//定义运动面积

int gameStat = 1; //用于记录游戏控制状态, 1为在控制, -1为不可控制

struct foodBall{	//定义食物结构体
	Vector3 pos;
	float r;
	Color color;
};
vector<foodBall> food_ball; 	//定义食物集


/*-----------------函数--------------------*/
void ControlBall_KeyBord(float scale);	//定义键盘控制函
void ControlBall_Mouse(float scale);	//定义鼠标控制函数
void Ball_Food_Init(int num);			//定义初始化食物数据函数
bool isEatCheck(foodBall food);			//检测是否吃到食物函数


/*-----------------主函数------------------------*/
int main(void)
{
	SetConfigFlags(FLAG_MSAA_4X_HINT | FLAG_WINDOW_RESIZABLE );//增加抗锯且可调整窗口大小
	InitWindow(1000, 800, "Ball War 3D");		//初始化窗口

	camera.position = (Vector3){100, 20, 0};	//设置观测点初始位置
	camera.target = (Vector3){0, 0, 0};			//设置观测点观测目标
	camera.up = (Vector3){0, 1, 0};				//设置相机的姿态
	camera.fovy = 40;							//设置相机的缩放效果

	HideCursor();			//隐藏光标

	ballPos = {0, 4, 0};	//设置小球初始位置
	ballR = 4;				//设置小球初始半径

	Ball_Food_Init(1000);	//初始化1000个食物数据
	SetTargetFPS(60);		//设置最高帧率为60

	while(1){
		DrawFPS(0, 0);		//显示帧率

		if(IsKeyPressed(KEY_ENTER)){  	//点击回车时切换游戏模式和设置模式
			gameStat = -gameStat;
			if(gameStat==-1) ShowCursor();	//设置模式显示鼠标
			else HideCursor();				//游戏模式隐藏鼠标
		}

		if(gameStat==1){
			ControlBall_KeyBord(1);	//调用键盘控制，速度为1
			ControlBall_Mouse(40);	//调用鼠标控制，灵敏度为40
		}

		camera.target = (Vector3){ballPos.x, ballPos.y+ballR*5/3, ballPos.z};	//更新观测目标位置

		BeginDrawing();	//开始绘画
		ClearBackground(BLACK); //清空窗口，设置背景为黑色
		BeginMode3D(camera);	//以摄像机视角绘制3d内容

		DrawGrid(50, 20);	//绘制水平面网格50x50个格子，每格长为20
		DrawSphere(ballPos, ballR, BLUE);	//绘制控制的球

		for(int i=0; i<=food_ball.size(); i++){		//逐步绘制食物位置

			Vector2 xyScreen = GetWorldToScreen(food_ball[i].pos, camera);		//获取食物在屏幕上投影位置
			float d = (food_ball[i].pos.x-ballPos.x)*(food_ball[i].pos.x-ballPos.x)+(food_ball[i].pos.z-ballPos.z)*(food_ball[i].pos.z-ballPos.z); //获取食物距离球的距离平方
			if(xyScreen.x >=0 && xyScreen.x<=GetScreenWidth() && xyScreen.y>=0 && xyScreen.y<=GetScreenHeight() && d<=250000) 	//判断食物是否在在可见范围并且距离不是很远
			{																													//目的减少绘制量提高帧率
				if(isEatCheck(food_ball[i])) //判断是否与食物碰撞
				{
					ballR += food_ball[i].r*1/5;		 //碰撞则半径加大
					food_ball.erase(begin(food_ball)+i); //碰撞则去掉这食物数据
				}
				else DrawSphere(food_ball[i].pos, food_ball[i].r, food_ball[i].color);	//不碰撞则绘制该食物
			}
		}

		EndMode3D();
		EndDrawing();


		if(IsKeyPressed(KEY_ESCAPE)) break;	//点击ESC退出
	}

	WindowShouldClose();

	return 0;
}


/*-------------------------函数定义------------------*/
//键盘控制函数
void ControlBall_KeyBord(float scale)
{
	Ray cameraRay = GetMouseRay((Vector2){GetScreenWidth()*1.0f/2, GetScreenHeight()*1.0f/2}, camera);	//获取窗口中心在三维空间投影的射线
	Vector3 dir_L_R = Vector3Normalize( Vector3CrossProduct(cameraRay.direction, (Vector3){0, 1, 0}));	//利用叉乘获取观测点的左右方向
	Vector3 dir_F_B = Vector3Normalize( Vector3CrossProduct(dir_L_R, (Vector3){0, 1, 0}));				//利用叉乘获取观测点的前后方向

	if(IsKeyDown(KEY_D))	//D被按下时执行
	{
		ballPos = Vector3Add(ballPos, Vector3Scale(dir_L_R, scale));	//小球右移
		camera.position = Vector3Add(camera.position, Vector3Scale(dir_L_R, scale)); //镜头右移
	}
	if(IsKeyDown(KEY_A))	//A被按下时执行
	{
		ballPos = Vector3Subtract(ballPos, Vector3Scale(dir_L_R, scale));	//小球左移
		camera.position = Vector3Subtract(camera.position, Vector3Scale(dir_L_R, scale));//镜头左移
	}
	if(IsKeyDown(KEY_W))	//w被按下时执行
	{
		ballPos = Vector3Subtract(ballPos, Vector3Scale(dir_F_B, scale));	//小球前移
		camera.position = Vector3Subtract(camera.position, Vector3Scale(dir_F_B, scale));	//镜头前移
	}
	if(IsKeyDown(KEY_S))	//S被按下时执行
	{
		ballPos = Vector3Add(ballPos, Vector3Scale(dir_F_B, scale));	//小球后移
		camera.position = Vector3Add(camera.position, Vector3Scale(dir_F_B, scale));	//镜头后移
	}
}

//鼠标控制函数
void ControlBall_Mouse(float scale)
{
	if(GetMouseX()!=GetScreenWidth()/2 && GetMouseY()!=GetScreenHeight()/2){	//鼠标不在窗口中心时执行
		Ray cameraRay = GetMouseRay(GetMousePosition(), camera);									//获取鼠标移动时在三维空间投影的射线
		camera.position = Vector3Subtract(camera.target, Vector3Scale(cameraRay.direction, scale));	//使得观测点与鼠标移动的位置重合
	}

	SetMousePosition(GetScreenWidth()/2, GetScreenHeight()/2);	//使鼠标移动到窗口中心
	UpdateCamera(&camera);	//更新相机
}

//初始化食物小球
void Ball_Food_Init(int num)
{
	time_t now = time(0);	//获取时间戳
	SetRandomSeed(now);		//设置时间戳为随机数种子

	for(int i=0;i<num;i++){
		float r = GetRandomValue(2, 5);							//随机获取食物的半径
		float x = GetRandomValue(r-maxSize.x/2, maxSize.x/2-r);	//随机获取食物球心x坐标
		float z = GetRandomValue(r-maxSize.y/2, maxSize.y/2-r);	//随机获取食物球心z坐标
		float y = r;											//设置球心y坐标与半径一样

		unsigned char color_r = GetRandomValue(0, 255);			//随机获取颜色r值
		unsigned char color_g = GetRandomValue(0, 255);			//随机获取颜色g值
		unsigned char color_b = GetRandomValue(0, 255);			//随机获取颜色b值
		unsigned char color_a = 255;							//设置透明度为255

		//增加属性到变量中
		food_ball.push_back( (foodBall){(Vector3){x,y,z}, r, (Color){color_r,color_g,color_b,color_a}} );
	}
}

//吃食物判断
bool isEatCheck(foodBall food)
{
	float d = Vector3Length(Vector3Subtract(food.pos, ballPos)); //获取球与食物球心距离
	if(ballR > food.r && d<ballR) return true;	//距离大于食物半径且球比食物大时为“可吃”
	else return false;
}
