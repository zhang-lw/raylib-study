#include <raylib.h>
#include <raymath.h>

int main(void)
{
	//初始化窗口
	InitWindow(1920, 1080, "RayLib-3D");

	// 初始化摄像机
	Camera3D camera;
	camera.position = (Vector3){ 40.0f, 20.0f, 0.0f }; 	//相机所在位置{x,y,z}
	camera.target = (Vector3){0.0f, 1.0f, 0.0f};  		//相机朝向位置{x,y,z}
	camera.up = (Vector3){ 0.0f, 2.0f, 0.0f }; 			//相机正上方朝向矢量
	camera.fovy = 60.0f; 						//相机视野宽度
	camera.projection = CAMERA_PERSPECTIVE; 	//采用透视投影
	SetCameraMode(camera, CAMERA_FIRST_PERSON); //使用第一人称

	SetTargetFPS(60);					//设置动画帧率
	SetConfigFlags(FLAG_MSAA_4X_HINT); 	//启用反锯齿

	// 绘画循环
	while (!WindowShouldClose())    //关闭窗口或者按ESC键时返回true
	{
		DrawFPS(0, 0);
		
		BeginDrawing();
		ClearBackground(WHITE); //清空窗口，设置背景颜色
		BeginMode3D(camera);	//以摄像机视角绘制3d内容
		
		DrawGrid(100, 5);	//绘制水平面网格
		
		DrawCube(Vector3{0,0,0},10,10,10,VIOLET);		//绘制立方体
		DrawCubeWires(Vector3{0,0,0},10,10,10,BLACK);	//正方体描边
		DrawLine3D(Vector3{0,0,0}, Vector3{0,30,0}, RED);
		
		
		EndMode3D();
		EndDrawing();
		
		UpdateCamera(&camera);
		
		if(GetKeyPressed()==KEY_ESCAPE) break;
	}

	//关闭窗口
	CloseWindow();

	return 0;
}
